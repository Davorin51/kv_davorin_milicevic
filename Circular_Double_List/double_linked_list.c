#include "double_linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

typedef struct linked_list node;
node *head = NULL;
node *tail = NULL;

static int capacity;

struct linked_list* init(){

	node *temp = (node*)malloc(sizeof(node));

	if(temp == NULL){
		printf("Memorija se ne može alocirati.\n");
		return NULL;
	}
	temp->previous = NULL;
	temp->next = NULL;

	return temp;

}

void insert_at_head(int number){
	node *newNode = init();

	newNode->number = number;
	newNode->next = newNode;
	newNode->previous = newNode;
	if(head == NULL){
		head = newNode;
		tail = newNode;
	}

	else{
		newNode->next = head;
		newNode->previous = tail;
		head->previous = newNode;
		tail->next = newNode;
		head = newNode;
	}
}
void insert_at_tail(int number)
{
    node *newNode = (node *) malloc(sizeof(node));

    newNode->number = number;
    newNode->next = newNode;
    newNode->previous = newNode;

    if(head==NULL)
    {
        head = newNode;
        tail = newNode;
    }
    else
    {
        tail->next = newNode;
        newNode->next = head;
        newNode->previous = tail;
        tail = newNode;
        head->previous = tail;
    }
}
	void print_list()
	{
	    printf("print:\n");
	    print_forward_order();

	}
	void print_forward_order()
	{
	    if(head==NULL)  return;

	    node *current = head;

	    do
	    {
	        printf("%d ", current->number);
	        current = current->next;
	    }   while(current != head);

	    printf("\nList Length: %d\n", getListLength());
	}

	int getListLength()
	{
	    if(head==NULL) return 0;

	    //int count = 0;
	    node *current = head;

	    do
	    {
	        capacity++;
	        current = current->next;
	    }   while(current != head);

	    return capacity;
	}

/*
int isEmpty(){
	return head == NULL;
}

int isFull(){
	if(tail->next == head){
		return 1;
	}
	else{
		return 0;
	}
}
int list_size()
{
    if (head == NULL || tail == NULL) {
        return 0;
    }

    struct node* temp = head;
    int count = 0;

    do {
        count += 1;
        temp = temp->next;
    } while (temp != head);

    return count;
}

struct node* init(){

	struct node* temp = (struct node*)malloc(sizeof(struct node));

	if(temp == NULL){
		printf("Memorija se ne može alocirati.\n");
		return NULL;
	}
	temp->previous = NULL;
	temp->next = NULL;

	return temp;

}
void insert_begin(int data)
{
    struct node* temp = init(data);

    if (temp)
    {
        // if list is empty
        if (head == NULL)
        {
            temp->next = temp;
            temp->previous = temp;
            head = temp;
            tail = temp;

        }
        head->previous->next = temp;
        temp->previous = head->previous;
        temp->next = head;
        head->previous = temp;
        head = temp;
    }
}

void insert_end(int data){
	struct node* temp = init(data);

	if(temp){
		if(head==NULL){
			temp->next = temp;
			temp->previous = temp;
			head = temp;
			tail = temp;
		}
		 head->previous->next = temp;
		 temp->previous = head->previous;
         temp->next = head;
         head->previous = temp;
	}
}

void insert_nth(int data, int position){
	struct node* temp = init(data);
	 if (position <= 0)
	    {
	        printf("\nInvalid Position\n");
	    } else if (head == NULL && position > 1) {
	        printf("\nInvalid Position\n");
	    } else if (head != NULL && position > list_size()) {
	        printf("\nInvalid Position\n");
	    } else if (position == 1) {
	        insert_begin(data);
	    }
}
*/
