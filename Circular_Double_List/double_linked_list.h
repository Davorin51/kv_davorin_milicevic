//#ifndef DOUBLE_LINKED_LIST_H_
//#define DOUBLE_LINKED_LIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>


void insert_at_head(int number);
void insert_at_tail(int number);
void insert_at_middle(int number, int position);
void delete_head();
void delete_tail();
void delete_middle(int posision);
void print_forward_order();
void print_reverse_order();
void print_list();
int getListLength();
struct linked_list{
    int number;
    struct linked_list *next;
    struct linked_list *previous;
};
/*
struct Node {
    int data;
    struct Node* link;
};

struct Queue {
    struct Node *head, *tail;
    int capacity;
};

void enQueue(Queue* q, int value)
{
    struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
    temp->data = value;
    if (q->front == NULL)
        q->front = temp;
    else
        q->rear->link = temp;

    q->rear = temp;
    q->rear->link = q->front;
}

// Function to delete element from Circular Queue
int deQueue(Queue* q)
{
    if (q->front == NULL) {
        printf("Queue is empty");
        return INT_MIN;
    }

    // If this is the last node to be deleted
    int value; // Value to be dequeued
    if (q->front == q->rear) {
        value = q->front->data;
        free(q->front);
        q->front = NULL;
        q->rear = NULL;
    }
    else // There are more than one nodes
    {
        struct Node* temp = q->front;
        value = temp->data;
        q->front = q->front->link;
        q->rear->link = q->front;
        free(temp);
    }

    return value;
}

// Function displaying the elements of Circular Queue
void displayQueue(struct Queue* q)
{
    struct Node* temp = q->front;
    printf("Elements in Circular Queue are: \n");
    while (temp->link != q->front) {
        printf("%d\n", temp->data);
        temp = temp->link;
    }
    printf("%d", temp->data);
}

/* Driver of the program */
/*
int main()
{
    // Create a queue and initialize front and rear
    Queue* q = (struct Queue*)malloc(sizeof(Queue));
    q->front = q->rear = NULL;

    // Inserting elements in Circular Queue
    enQueue(q, 14);
    enQueue(q, 22);
    enQueue(q, 6);

    // Display elements present in Circular Queue
    displayQueue(q);

    // Deleting elements from Circular Queue
    printf("Deleted value: %d\n", deQueue(q));
    printf("Deleted value: %d\n", deQueue(q));

    // Remaining elements in Circular Queue
    displayQueue(q);

    enQueue(q, 9);
    enQueue(q, 20);
    displayQueue(q);

    return 0;
}
*/
*/

